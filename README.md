# RdvPrefecture
Un script Python pour vérifier les rendez-vous disponibles sur le site de la préfecture


RDVPref is a Python script that will help you book a RDV in a french "prefecture". It can be run on the following systems:
  - Linux
  - Windows 
  - MAC

= Revision history =
- `v1: CD, 25/05/19: The v1 of the script is using requests.post/get API
- `v2: CD, 15/10/21: The v2 of the script is using chromedriver API

= Configuration =

 RDVPref provides a configuration file named credentials.py.
 It contains the login and the password of the user's email account.\n
 Those information are needed to send notifications to the user through SMTP. \n

 credentials.py file:\n
     | EMAIL_ADDRESS :  The email address of the user            
     | PASSWORD : 	    The password of the email account
	 
	 
= Run =
 
	python3 -m pip install --upgrade pip
	python3 -m pip install bs4
	python3 -m pip install lxml	
	run python3.9 .\main.py
	

= Limitations =
The following limitations are specific to this version of the script: 
- This script was designed for Gmail accounts and Python3
