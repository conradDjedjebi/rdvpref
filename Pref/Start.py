__title__   =   'Book an appointment in any Prefecture'
__version__ =   '0.2.0'
__date__    =   "15/10/2021"
__author__  =   'C.D'

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
PATH = "C:\Program Files (x86)\Google\Chrome\chromedriver_win32\chromedriver.exe"
driver = webdriver.Chrome(PATH)

from .credentials import *
import requests
import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from time import sleep
from random import randint
try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

PLANNING       = ["planning23018", "planning23198", "planning23199", "planning23200", "planning23201"]
GUICHET_NAME = ["A"            , "B"            , "C"            , "D"            , "E"            ]

noRdv="Il n'existe plus de plage horaire libre pour votre demande de rendez-vous. Veuillez recommencer"
noRdvEn="no planning"
BASE_URL = "http://www.essonne.gouv.fr/booking/create/23014/0"

#Main + Loop
def Start():
    print("[OK] Starting Bot at: "+str(datetime.datetime.now()))
    alertSent = False
    #while alertSent == False:
    while 1:    
        alertSent = check_rdv(BASE_URL)
        #alertSent = alert_tester(BASE_URL, "planning23018", "A") or alertSent#Alert tester        
            
        time = datetime.datetime.now()
        print("[OK] Last Check at: "+str(time))

        sleep(60)# Chaque 60s
        #sleep(60*60)# Chaque heure

#Check RDV : x in rang 1-5 replace planning send parse res alert Y/N
def check_rdv(base_url):
    alertSent = False
    for i in range(0,5,1):
        try:
        
            print("[ROBOT] Checking Guichet "+GUICHET_NAME[i]+ " ...")         
            guichetId = (PLANNING[i])
            output = check_website(base_url, guichetId)
            if parser(output):
                alert(base_url, GUICHET_NAME[i]) #Send message
                alertSent = True
                print("[ROBOT] Success! RDV Found")                   
                print("[ROBOT] RDV Found at "+ base_url + "Guichet " + GUICHET_NAME[i])                 
            else:
                print("[Skip] Guichet["+GUICHET_NAME[i]+"]")
            
        except Exception as e:
            print(e)
            
        rand = 30+randint(0, 30)
        print("[OK] Next Guichet in seconds: "+str(rand))
        sleep(rand)
    return alertSent

#Check Sur rendez vous
def check_surrdv(base_url):
    output = check_website(base_url, guichetId)
    alertSent = False
    if parser(output):
        alert(base_url,0)
        alertSent = True
    else:
        print("[Skip] Sur Reservation")
    return alertSent

#Parsing HTML
def parser(text):
    parsed_html = BeautifulSoup(text,"lxml")
    try:
        result = parsed_html.body.find('form', attrs={'id':'FormBookingCreate'}).text.strip()
        print(result)
        if (noRdv in result) or (noRdvEn in result):
            print("noRdv MSG FOUND")
            return False        
        else:
            return True        
    except Exception as e:
        print(e)    
        return False

#Alert SMS ou Email
def alert(url,guichet):
    print("[OK] Sending Alert")
    if guichet==-1:
        msg ="Test http://google.com"
        subject = "Test"
    elif guichet == 0:
        msg = "Rendez-vous disponible à l'adresse "+url
        subject ="Sur Reservation"
    else:
        msg = "Rendez vous dispo sur le guichet "+str(guichet)+" a l'adresse : "+url
        subject ="Guichet "+str(guichet)
    sendMail(msg,subject)

def sendMail(content,subject):
    print("[OK] Sending eMail")
    try:
        fromaddr = "noreply.ordo@gmail.com"
        toaddr = EMAIL_ADDRESS
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "[Pref-Alert] "+subject

        body = content
        msg.attach(MIMEText(body, 'plain'))

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        server.login(EMAIL_ADDRESS, PASSWORD)
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        print("Success: Email sent!")
    except:
        print("Email failed to send.")

#alert tester
def alert_tester(base_url, guichetId, guichetName):
    print("[TEST] START TEST")
   
    alertSent = False
    output = check_website(base_url, guichetId)
    
    if parser(output):
        #alert(base_url, -1)
        alert(base_url, guichetName)
        alertSent = True
        print("[TEST] Success!")        
    else:
        print("[TEST] Failed Test")
    print("[Test] END TEST")
    return alertSent

#check website
def check_website(base_url, guichetId):
    output = ""
    driver.get(base_url)    
    try:  
        #accept conditions
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.NAME, "condition"))
        )    
        rand = randint(0, 2)
        sleep(rand)             
        element.click()   
        
        #Go to next page
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.NAME, "nextButton"))
        ) 
        rand = randint(0, 2)
        sleep(rand)        
        element.click()
    
        #Select Guichet
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, guichetId))
        )  
        rand = randint(0, 2)
        sleep(rand)     
        element.click()   

        #Go to next page
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.NAME, "nextButton"))
        )    
        rand = randint(0, 2)
        sleep(rand)             
        element.click()
        
        time.sleep(2)
        output = driver.page_source.encode('utf-8')

    except Exception as e:
        print(e)    
        #driver.quit()  
        
    return output                

Start()
#if __name__== "__main__":
    #main()